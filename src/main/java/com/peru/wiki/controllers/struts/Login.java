package com.peru.wiki.controllers.struts;

public interface Login<T> {
	T login(T... value);
}
