package com.peru.wiki.controllers.struts;

import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.peru.wiki.models.User;
import com.peru.wiki.sql.GenericDao;

@SuppressWarnings("serial")
@ParentPackage("json-default")
@Results(value = { @Result(name = "home_json", type = "json") })
public class LoginCtrl extends ActionSupport implements SessionAware {

	private Map<String, Object> session;
	private String email;
	private String password;

	private final String HOME_JSON = "home_json";
	private User user;
	private GenericDao<User, Integer> dao = new GenericDao<User, Integer>(User.class);
	
	@Override
	public String execute() throws Exception {
		return super.execute();
	}

	@Action(value = "login")
	public String doLogin() {
		Login<String> log=(params)->{
			user = dao.findAllByField(params[0], params[1]).get(0);
			if (user != null && user.getPassword().equals(password)) {
				session.put("user", user);
			}
			return HOME_JSON; 
		};
		
		return log.login("email", email);
	}

	@Action(value = "logup")
	public String doLogup() {

		dao.create(user);
		session.put("user", user);

		return HOME_JSON;

	}

	@Action(value = "logout")
	public String doLogout() {
		session.clear();
		return HOME_JSON;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
