package com.peru.wiki.controllers.struts;

import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.peru.wiki.models.User;
import com.peru.wiki.sql.GenericDao;

@SuppressWarnings("serial")
@ParentPackage("json-default")
@Results(value = { @Result(name = "disp_users_json", type = "json"),
		@Result(name = "disp_user", location = "/pages/users/displayUser.jsp"),
		@Result(name = "home", location = "/pages/home.jsp") })
public class UserCtrl extends ActionSupport implements SessionAware {

	private final String DISPLAY_USERS_JSON = "disp_users_json";
	private final String DISPLAY_USER = "disp_user";
	private final String HOME = "home";

	private Map<String, Object> session;
	private List<User> users;
	private User user;
	private int user_ID;
	private String searchInput;
	private String from;
	private GenericDao<User, Integer> dao = new GenericDao<User, Integer>(User.class);

	@Action(value = "updateUser")
	public String doUpdate() {

		dao.update(user);

		if (from.equals("profile")) {
			session.put("user", dao.findById(((User) session.get("user")).getId()));
			return doEdit();
		} else {
			return doList();
		}

	}

	@Action(value = "findUser")
	public String doFind() {
		return HOME;
	}

	@Action(value = "createUser")
	public String doCreate() {
		return HOME;
	}

	@Action(value = "deleteUser")
	public String doDel() {
		User sessionUser = (User) session.get("user");

		if (user_ID != sessionUser.getId()) {
			dao.deleteById(user_ID);
		}
		return doList();
	}

	@Action(value = "saveUser")
	public String doSave() {

		dao.create(user);

		return HOME;
	}

	@Action(value = "listUser")
	public String doList() {

		users = dao.findAll();

		return DISPLAY_USERS_JSON;
	}

	@Action(value = "editUser")
	public String doEdit() {
		return DISPLAY_USER;
	}

	@Action(value = "searchUser")
	public String doSearch() {

		users = dao.findAllBySearch(searchInput);
		if (users != null) {
			return DISPLAY_USERS_JSON;
		} else {
			return HOME;
		}
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getUser_ID() {
		return user_ID;
	}

	public void setUser_ID(int user_ID) {
		this.user_ID = user_ID;
	}

	public String getSearchInput() {
		return searchInput;
	}

	public void setSearchInput(String searchInput) {
		this.searchInput = searchInput;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

}
