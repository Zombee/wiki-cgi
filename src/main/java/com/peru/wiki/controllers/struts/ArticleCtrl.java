package com.peru.wiki.controllers.struts;

import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.peru.wiki.models.Article;
import com.peru.wiki.models.Roles;
import com.peru.wiki.models.User;
import com.peru.wiki.sql.GenericDao;

@SuppressWarnings("serial")
@ParentPackage("json-default")
@Results(value = { @Result(name = "disp_arts_json", type = "json"),
		@Result(name = "disp_art", location = "/pages/displayArticle.jsp"),
		@Result(name = "form_art", location = "/pages/articleForm.jsp"),
		@Result(name = "home", location = "/pages/home.jsp") })
public class ArticleCtrl extends ActionSupport implements SessionAware {

	private final String DISPLAY_ARTICLES_JSON = "disp_arts_json";
	private final String DISPLAY_ARTICLE = "disp_art";
	private final String FORM_ARTICLE = "form_art";
	private final String HOME = "home";

	private Map<String, Object> session;
	private List<Article> articles;
	private Article article;
	private int article_ID;
	private int author_ID;
	private User author;
	private String searchInput;
	private GenericDao<Article, Integer> dao = new GenericDao<Article, Integer>(Article.class);
	private GenericDao<User, Integer> userDao = new GenericDao<User, Integer>(User.class);


	@Action("listArticle")
	public String doList() {

		User user = (User) session.get("user");

		if (user.getRole() == Roles.ADMIN) {
			articles = dao.findAll();
		} else {
			articles = dao.findAllByField("author_ID", user.getEmail());
		}

		return DISPLAY_ARTICLES_JSON;
	}

	@Action("searchArticle")
	public String doSearch() {

		articles = dao.findAllBySearch(searchInput);

		if (articles != null) {
			return DISPLAY_ARTICLES_JSON;
		} else {
			return HOME;
		}
	}

	@Action("editArticle")
	public String doEdit() {

		setArticle((Article) dao.findById(article_ID));
		return FORM_ARTICLE;
	}

	@Action("createArticle")
	public String doCreate() {
		return HOME;
	}

	@Action("findArticle")
	public String doFind() {
		article = dao.findById(article_ID);
		setAuthor(userDao.findById(author_ID));

		return DISPLAY_ARTICLE;
	}

	@Action("saveArticle")
	public String doSave() {
		return HOME;
	}

	@Action("deleteArticle")
	public String doDel() {

		dao.deleteById(article_ID);

		return doList();
	}

	@Action("updateArticle")
	public String doUpdate() {

		for (Article art : articles) {
			dao.update(art);
		}

		return doList();
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public int getArticle_ID() {
		return article_ID;
	}

	public void setArticle_ID(int article_ID) {
		this.article_ID = article_ID;
	}

	public int getAuthor_ID() {
		return author_ID;
	}

	public void setAuthor_ID(int author_ID) {
		this.author_ID = author_ID;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public String getSearchInput() {
		return searchInput;
	}

	public void setSearchInput(String searchInput) {
		this.searchInput = searchInput;
	}

}
