package com.peru.wiki.controllers.struts;

import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.peru.wiki.models.Comment;
import com.peru.wiki.models.Roles;
import com.peru.wiki.models.User;
import com.peru.wiki.sql.GenericDao;

@SuppressWarnings("serial")
@ParentPackage("json-default")
@Results(value = { @Result(name = "home", location = "/pages/home.jsp"),
		@Result(name = "disp_coms_json", type = "json") })
public class CommentCtrl extends ActionSupport implements SessionAware {

	private final String HOME = "home";
	private final String DISPLAY_COMMENTS_JSON = "disp_coms_json";

	private Map<String, Object> session;

	private List<Comment> comments;
	private String searchInput;
	private GenericDao<Comment, Integer> dao = new GenericDao<Comment, Integer>(Comment.class);

	@Action(value = "listComment")
	public String doList() {

		User user = (User) session.get("user");

		if (user.getRole() == Roles.ADMIN) {
			setComments(dao.findAll());
		} else {
			setComments(dao.findAllByField("author_ID", user.getEmail()));
		}

		return DISPLAY_COMMENTS_JSON;
	}

	@Action(value = "searchComment")
	public String doSearch() {

		comments = dao.findAllBySearch(searchInput);
		if (comments != null) {
			return DISPLAY_COMMENTS_JSON;
		} else {
			return HOME;
		}
	}

	@Action(value = "editComment")
	public String doEdit() {
		return HOME;
	}

	@Action(value = "createComment")
	public String doCreate() {
		return HOME;
	}

	@Action(value = "findComment")
	public String doFind() {
		return HOME;
	}

	@Action(value = "saveComment")
	public String doSave() {
		return HOME;
	}

	@Action(value = "delComment")
	public String doDel() {
		return HOME;
	}

	@Action(value = "updateComment")
	public String doUpdate() {
		return HOME;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public String getSearchInput() {
		return searchInput;
	}

	public void setSearchInput(String searchInput) {
		this.searchInput = searchInput;
	}

}
