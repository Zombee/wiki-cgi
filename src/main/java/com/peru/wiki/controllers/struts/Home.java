package com.peru.wiki.controllers.struts;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
@Action(value="home")
@Result(name="success", location="/pages/header.jsp")
public class Home extends ActionSupport {
	
	@Override
	public String execute() throws Exception {
		return super.execute();
	}	
}
