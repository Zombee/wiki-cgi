package com.peru.wiki.models;

public enum Status {
	BANISHED,
	SUSPENDED,
	CLEARED;
}
