package com.peru.wiki.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@SuppressWarnings("serial")
@Entity
@Table(name="comment")
public class Comment implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="author_ID", updatable = false, insertable = false, nullable=false)
	private User author;
	
	@Column(name="content", nullable=false)
	private String content;
	
	@ManyToOne
	@JoinColumn(name="article_ID", updatable = false, insertable = false, nullable=false)
	private Article article;
	
	@ManyToOne
	@JoinColumn(name="comment_ID", updatable = false, insertable = false)
	private Comment parentCom;
	
	@OneToMany(mappedBy="parentCom", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Comment> childCom;
	
	@Column(name="date", nullable=false)
	private String date;
	
	@Column(name="score", nullable=false)
	private int score;
	
	public Comment() { 	}
	
	public Comment(String content, User author, Article article, Comment parentCom)
	{
		this(content, author, article, parentCom, new Date().toString(), 0);
	}
	
	public Comment(String content, User author, Article article, Comment parentCom, String date, int score)
	{
		setAuthor(author);
		setContent(content);
		setArticle(article);
		setParentCom(parentCom);
		setDate(date);
		setScore(score);
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Comment getParentCom() {
		return parentCom;
	}

	public void setParentCom(Comment parentCom) {
		this.parentCom = parentCom;
	}
	
	public List<Comment> getChildCom() {
		return childCom;
	}

	public void setChildCom(List<Comment> childCom) {
		this.childCom = childCom;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
}
