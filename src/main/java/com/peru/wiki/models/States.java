package com.peru.wiki.models;

public enum States
{
	DECLINED,
	PENDING,
	VALIDATED;
}
