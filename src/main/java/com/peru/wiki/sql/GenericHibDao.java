//package com.peru.wiki.sql;
//
//import java.io.Serializable;
//import java.util.List;
//
//import org.hibernate.HibernateException;
//import org.hibernate.Query;
//import org.hibernate.Session;
//import org.hibernate.Transaction;
//
//import com.peru.wiki.util.DataAccessLayerException;
//import com.peru.wiki.util.HibernateFactory;
//
//public class GenericHibDao<T, PK extends Serializable> {
//
//	protected static Session session;
//	private Transaction tx;
//
//	private Class<T> entityClass;
//
//	public GenericHibDao(Class<T> obj) {
//		entityClass = obj;
//	}
//
//	public void saveOrUpdate(T obj) {
//		try {
//			startOperation();
//			session.saveOrUpdate(obj);
//			tx.commit();
//		} catch (HibernateException e) {
//			handleException(e);
//		} finally {
//			HibernateFactory.close(session);
//		}
//	}
//
//	public void delete(T obj) {
//		try {
//			startOperation();
//			session.delete(obj);
//			tx.commit();
//		} catch (HibernateException e) {
//			handleException(e);
//		} finally {
//			HibernateFactory.close(session);
//		}
//	}
//
//	public void deleteById(PK id) {
//		T obj = findById(id);
//		delete(obj);
//	}
//
//	public T findById(PK id) {
//		T obj = null;
//		try {
//			startOperation();
//			obj = session.load(entityClass, id);
//			tx.commit();
//		} catch (HibernateException e) {
//			handleException(e);
//		} finally {
//			HibernateFactory.close(session);
//		}
//		return obj;
//	}
//
//	public List<T> findAll() {
//		List<T> objects = null;
//		try {
//			startOperation();
//			Query query = session.createQuery("from " + entityClass.getName());
//			objects = query.list();
//			tx.commit();
//		} catch (HibernateException e) {
//			handleException(e);
//		} finally {
//			HibernateFactory.close(session);
//		}
//		return objects;
//	}
//
//	public List<T> findAllByField(String field, String value) {
//		List<T> objects = null;
//		try {
//			startOperation();
//			Query query = session.createQuery("from " + entityClass.getName() + " where " + field + "=" +value);
//			System.out.println(query.toString());
//			objects = query.list();
//			tx.commit();
//		} catch (HibernateException e) {
//			handleException(e);
//		} finally {
//			HibernateFactory.close(session);
//		}
//		return objects;
//	}
//
//	public List<T> findAllBySearch(String input) {
//
//		return null;
//	}
//	
//	protected void handleException(HibernateException e) throws DataAccessLayerException {
//		HibernateFactory.rollback(tx);
//		throw new DataAccessLayerException(e);
//	}
//
//	protected void startOperation() throws HibernateException {
//		session = HibernateFactory.openSession();
//		tx = session.beginTransaction();
//	}
//
//}
