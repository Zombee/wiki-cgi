package com.peru.wiki.sql;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class GenericDao<T, PK> {

	protected Class<T> entityClass;

	protected static EntityManager entityManager;

	public GenericDao(Class<T> obj) {
		this.entityClass = obj;
		
		if (entityManager == null) {
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("wiki3");
			entityManager = emf.createEntityManager();
		}
	}

	public T create(T obj) {
		entityManager.persist(obj);
		return obj;
	}

	public T findById(PK id) {
		return entityManager.find(entityClass, id);
	}

	public T update(T obj) {
		return entityManager.merge(obj);
	}

	public void delete(T obj) {
		entityManager.remove(obj);
	}

	public void deleteById(PK id) {
		T obj = entityManager.find(entityClass, id);
		delete(obj);
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		Query q = entityManager.createQuery("SELECT o FROM " + entityClass.getName() + " o");
		return q.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<T> findAllByField(String field, String value) {
		
		Query q = entityManager.createQuery(
				"SELECT o FROM " + entityClass.getName() + " o WHERE o." + field + " = :value");
		q.setParameter("value", value);
		return q.getResultList();
	}

	public List<T> findAllBySearch(String input) {

		return null;
	}
}
