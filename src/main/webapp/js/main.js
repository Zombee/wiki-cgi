var templates = {};

function displayNavbar() {
	$.ajax({
		method : "GET",
		url : "test",
		dataType : "json"
	}).done(function(response) {
		if (response.session.user != null) {
			$(".logout, .list-user, .list-article, .list-comment, .edit-user").removeClass("hidden");
			$("#btn-login-modal, #btn-logup-modal, #loginModal").hide();
			$('.modal-backdrop').remove();
		}
	});
}

displayNavbar();

function display_template(tpl, data, target) {
	if (templates[tpl] === undefined) {
		return;
	}
	
	var template = templates[tpl];
	var html = template(data);
	target.empty();
	target.html(html);
}

$(document).ready(function() {

	$(".checkAll").on("click",function() {
		$('input:checkbox').not(this).prop('checked', this.checked);
	});

	//============================
	// Connexion Action
	//============================
	$(".login").on("click", function() {
		var emailI = $("#email").val();
		var passwordI = $("#password").val();
		
		$.ajax({
			method : "POST",
			url : "login",
			dataType : "json",
			data : {
				email : emailI,
				password : passwordI
			}
		}).done(function(response) {
			console.log(response);
			if (response.session.user != null) {
				$("#login-form")[0].reset();
				$(".logout, .list-user, .list-article, .list-comment, .edit-user").removeClass("hidden");
				$("#btn-login-modal, #btn-logup-modal, #loginModal").hide();
				$('.modal-backdrop').remove();
			} else {
				$("#login-form")[0].reset();
				$("#login-error").removeClass("hidden");
			}
		});
		
	});
	
	$(".logout").on("click", function() {
		$.ajax({
			type : "GET",
			url : "logout",
			dataType : "json"
		}).done(function(res) {
			location.reload(true);
		});
	});
	
	
	$(".logup").on("click", function() {
		var userI = {};
		userI.role = $("#role").val();
		userI.username = $("#username").val();
		userI.email = $("#email").val();
		userI.password = $("#password").val();
		
		$.ajax({
			method : "POST",
			url : "logup",
			dataType : "json",
			data : {
				user : userI
			}
		}).done(function(response) {
			console.log(response);
			if (response.session.user != null) {
				$("#login-form")[0].reset();
				$(".logout, .list-user, .list-article, .list-comment, .edit-user").removeClass("hidden");
				$("#btn-login-modal, #btn-logup-modal, #loginModal").hide();
				$('.modal-backdrop').remove();
			} else {
				$("#login-form")[0].reset();
				$("#login-error").removeClass("hidden");
			}
		});
	});
	//============================
	// Content Action
	//============================
	
	$("body").on("click", "#search-button, .delete-user, .list-user, .list-article, .delete-article, .list-comment", function() {
		
		var urlAct= "";
		var dataAct = {};
		var name = "";
		var template = "";
		
		if($(this).hasClass("search")) {
			var opt = $("#search-option").val();
			
			if (opt === "Article") {
				urlAct = "searchArticle";
				name = "articles";
				template = "./templates/search.articles.handlebars";
			} else if (opt === "Comment") {
				urlAct = "searchComment";
				name = "comments";
				template = "./templates/search.comments.handlebars";
			} else if (opt === "User") {
				urlAct = "searchUser";
				name = "users";
				template = "./templates/search.users.handlebars";
			} 
			
			dataAct.searchInput = $("#search-input").val();
			
		} else if ($(this).hasClass("delete-user")) {
			urlAct = "deleteUser";
			dataAct.user_ID = $(this).closest("tr").attr("id");
			name = "users";
			template = "./templates/users.handlebars";
		} else if ($(this).hasClass("list-user")) {
			urlAct = "listUser";
			name = "users";
			template = "./templates/users.handlebars";
		} else if ($(this).hasClass("list-article")) {
			urlAct = "listArticle";
			name = "articles.admin";
			template = "./templates/articles.admin.handlebars";
		} else if ($(this).hasClass("delete-article")) {
			urlAct = "deleteArticle";
			dataAct.article_ID = $(this).closest("tr").attr("id");
			name = "articles";
			template = "./templates/articles.handlebars";
		} else if ($(this).hasClass("list-comment")) {
			urlAct = "listComment";
			name = "comments";
			template = "./templates/comments.handlebars";
		}
		
		$.ajax({
			method : "POST",
			url : urlAct,
			dataType : "json",
			data: dataAct
		}).done(function(response) {
			var target = $(".content");

			if (templates[name] === undefined) {
				$.get(template, function(res) {
					templates[name] = Handlebars.compile(res);
					display_template(name, response, target);
				});
			} else {
				display_template(name, response, target);
			}
		});
		
	});

});