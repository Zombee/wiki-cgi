<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	import="com.peru.wiki.models.Roles"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Profile</title>
</head>
<body>

	<jsp:include page="../header.jsp"></jsp:include>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<h1>Profile</h1>

				<form method="post" action="updateUser">
					<div class="form-group">

						<label>Role</label> 
						<select class="form-control" name="user.title">

							<c:set var="roles" value="<%=Roles.values()%>" />
							<c:forEach items="${roles}" var="role">
								<c:choose>
									<c:when test="${sessionScope.user.role == role}">
										<option value="${role}" selected>${role}</option>
									</c:when>
									<c:otherwise>
										<option value="${role}">${role}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>

						</select>
					</div>
					<div class="form-group">
						<label>Username</label> <input class="form-control" type="text"
							name="user.username" id="input" placeholder="Username"
							value="${sessionScope.user.username}" required >
					</div>
					<div class="form-group">
						<label>Email</label> <input class="form-control" type="text"
							name="user.email" id="input" placeholder="Email"
							value="${sessionScope.user.email}" required />
					</div>
					<div class="form-group">
						<label>Password</label> <input class="form-control"
							type="password" name="user.password" id="input"
							placeholder="New Password" />
					</div>
<%-- 					<input type=hidden name="action" value="${Actions.UPDATE}" /> --%>
					<input type=hidden name="from" value="profile" />
					<button type="submit" class="btn btn-default btn-success">Save Changes</button>
				</form>
			</div>
			<div class="col-md-4">
				<h2>Level: ${sessionScope.user.level}</h2>
				
				<a type="button" class="btn btn-primary" href="#">New Article</a>
			</div>
		</div>
	</div>

</body>
</html>