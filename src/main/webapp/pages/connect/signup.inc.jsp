<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	import="com.peru.wiki.models.Roles"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Sign Up</h4>
		</div>
		<div class="modal-body">
			<form method="post" action="saveUser">
				<div class="form-group">

					<label>Role</label> <select class="form-control" name="user.role">

						<c:set var="roles" value="<%=Roles.values()%>" />
						<c:forEach items="${roles}" var="role">
							<option value="${role}">${role}</option>
						</c:forEach>

					</select>
				</div>
				<div class="form-group">
					<label>Username</label> <input class="form-control" type="text"
						name="user.username" id="input" placeholder="Username" required />
				</div>
				<div class="form-group">
					<label>Email</label> <input class="form-control" type="text"
						name="user.email" id="input" placeholder="Email" required />
				</div>
				<div class="form-group">
					<label>Password</label> <input class="form-control" type="password"
						name="user.password" id="input" placeholder="Password" required />
				</div>
				<button type="submit" class="btn btn-default btn-success">Create
					Account</button>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>
</div>
