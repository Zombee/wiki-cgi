<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Sign In</h4>
		</div>
		<div class="modal-body">
			<form id="login-form">
				<div class="form-group">
					<label for="id">Email</label> 
					<input type="email" class="form-control" id="email"/>
				</div>
				<div class="form-group">
					<label for="Password">Password</label> 
					<input type="password" class="form-control" id="password"/>
				</div>
				<button type="button" class="btn btn-default btn-success login">Submit</button>
			</form>
		</div>
		<div class="alert alert-danger hidden" role="alert" id="loginError">Wrong Username or Password, try again.</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>
</div>