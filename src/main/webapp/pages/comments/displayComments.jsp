<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	import="com.peru.wiki.models.Status"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Comments List</title>
</head>
<body>
	<jsp:include page="../header.jsp"></jsp:include>

	<div class="container-fluid">
		<h1>Comments List</h1>
		<table class="table table-hover table-condensed"
			data-click-to-select="true">
			<thead>
				<tr>
					<th><input type="checkbox" class="checkAll"></th>
					<th>Author</th>
					<th>Content</th>
					<th>Article</th>
					<th>Comment</th>
					<th>Score</th>
					<th>Date</th>
					<th>Action</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th><input type="checkbox" class="checkAll"></th>
					<th>Author</th>
					<th>Content</th>
					<th>Article</th>
					<th>Comment</th>
					<th>Score</th>
					<th>Date</th>
					<th>Action</th>
				</tr>
			<tfoot>
			<tbody>
				<c:forEach items="${comments}" var="current">
					<tr  id="row">
						<td><input type="checkbox" value="${current.id}"></td>
						<td><c:out value="${current.authorId}" /></td>
						<td><c:out value="${current.content}" /></td>
						<td><c:out value="${current.articleId}" /></td>
						<td><c:out value="${current.commentId}" /></td>
						<td><c:out value="${current.score}" /></td>
						<td><c:out value="${current.date}" /></td>
						<td>
							<form method="post" action="/wiki/user">
								<button type="submit" class="btn btn-danger" name="">Delete</button>
<%-- 								<input type="hidden" name="action" value="${Actions.DELETE}"> --%>
								<input type="hidden" name="userId" value="${current.id}">
							</form>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<form method="post" action="/wiki/user">
			<button type="submit" class="btn btn-danger" name="">Delete</button>
<%-- 			<input type="hidden" name="action" value="${Actions.DELETE}"> --%>
		</form>
	</div>
</body>
</html>