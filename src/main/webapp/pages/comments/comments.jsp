<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<ul class="media-list">
	<c:forEach items="${comments}" var="comment">
		<li class="media">
			<div class="media-body">
				<div class="media-heading">
					<span class="comment-author">${comment.author}</span> 
					<span class="comment-score">${comment.score}</span>
				</div>
				<span class="comment-content">${comment.content}</span>
			</div>
			<div class="comment-create-reply">
				<div class="icon-reply"></div>
				<span>Reply</span>
			</div>
			<span></span>
		</li>
	</c:forEach>
</ul>
