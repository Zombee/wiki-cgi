<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.peru.wiki.models.States, com.peru.wiki.models.Roles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><c:out value='${article.title}'></c:out></title>
		<link rel="stylesheet" href="css/displayArticle.css" />
	</head>

	<body>
		<jsp:include page="header.jsp"></jsp:include>
		
		<section id="displayArticle_section">
			<div id="displayArticle_content">
				<header><c:out value='${article.title}'></c:out></header>
				<article id="displayArticle_article">
					<textarea class="form-control" rows="15"><c:out value='${article.content}'></c:out></textarea>
				</article>
				<footer id="displayArticle_footer">
					<p>Ranking: <c:out value='${article.score}'></c:out></p>
					<form method="post" action="/wiki/article" id="displayArticle_footer_form">
						<input type=hidden name="score" value="${article.score + 1}"/>
<%-- 						<input type=hidden name="action" value="${Actions.UPDATE}"/> --%>
						<input type="image" src="" id="score"/>
					</form>
				</footer>
			</div>
			
			<div id="displayArticle_asides">
				<aside class="displayArticle_asideContent">
					<h1>About the article</h1>
					<p>Title: <c:out value='${article.title}'></c:out></p>
					<p>Publication Date: <c:out value='${article.pubDate}'></c:out></p>
					<P>Ranking: <c:out value='${article.score}'></c:out></p>
				</aside>
				
				<aside class="displayArticle_aside">
					<h1>About the author</h1>
					<p>Nickname: <c:out value='${user.username}'/></p>
					<p>Email: <c:out value='${user.email}'/></p>
					<p>Rank: <c:out value='${user.role}'/></p>
					<p>Ranking: <c:out value='${user.level}'/></p>
				</aside>
			</div>
		</section>
	</body>
	
</html>
