<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.peru.wiki.models.States,
			com.peru.wiki.models.Roles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Wikis creation</title>
	</head>

	<body>
		<jsp:include page="header.jsp"></jsp:include>
	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-8">
					<h1>Wiki</h1>
	
					<c:set var="article" value="${article}"></c:set>
					<form method="post" action="/wiki/article">
					
						<div class="form-group">
							<label>Wiki's title</label> <input class="form-control"
								type="text" name="title" id="input" placeholder="Wiki's title..."
								value="<c:out value='${article.title}'/>" required>
						</div>
						
						<div class="form-group">
							<label>Publication date</label> <input class="form-control"
								type="text" name="pubDate" id="input" placeholder="Publication date..."
								value="<c:out value='${article.pubDate}'/>" required>
						</div>
	
						<div class="form-group">
							<label>Wiki's body</label>
							<textarea class="form-control" name="content" id="input" rows="10"
								required><c:out value='${article.content}'></c:out>
							</textarea>
						</div>
	
						<c:if test="${sessionScope.user.role == Roles.ADMIN}">
							<div class="form-group">
								<label>Wiki's state</label> <select class="form-control"
									name="states">
									<c:set var="states" value="<%=States.values()%>"/>
									<c:forEach items="${states}" var="stateFE">
										<c:choose>
											<c:when test="${article.state == stateFE}">
												<option value="${stateFE}" selected>${stateFE}</option>
											</c:when>
											<c:otherwise>
												<option value="${stateFE}">${stateFE}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>
							</div>
						</c:if>
	
<%-- 						<input type=hidden name="action" value="${Actions.UPDATE}"/> --%>
						<input type=hidden name="articleID" value="${article.id}"/>
						<input type=hidden name="writerID" value="${article.author_ID}"/>
						<input type=hidden name="score" value="${article.score}"/>
						<input type="submit" class="btn btn-default btn-success"/>
					</form>
				</div>
			</div>
		</div>
	</body>
	
</html>