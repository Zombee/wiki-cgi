<%@ page import="com.peru.wiki.models.Roles"%>
<link rel="stylesheet" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/style.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/handlebars-v4.0.5.js"></script>
<script src="./js/main.js"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="collapsed navbar-toggle"
				data-toggle="collapse" data-target=".navbar-collapse"
				aria-expanded="false">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<span class="navbar-brand">Wiki</span>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li><button class="btn btn-link home">Home</button></li>
				<li><button class="btn btn-link" data-toggle="modal" data-target="#loginModal" id="btn-login-modal">Sign In</button></li>
				<li><button class="btn btn-link" data-toggle="modal" data-target="#logupModal" id="btn-logup-modal">Sign Up</button></li>
				<li><button class="btn btn-link hidden list-user">Users Panel Control</button></li>
				<li><button class="btn btn-link hidden list-article">My Wikis</button></li>
				<li><button class="btn btn-link hidden list-comment">My Comments</button></li>
				<li><button class="btn btn-link hidden edit-user">Profile</button></li>
				<li><button class="btn btn-link hidden logout">Sign Out</button></li>
			</ul>
			<div class="navbar-form navbar-right" role="search">
				<div class="input-group">
					<div class="form-group">
						<input class="form-control" id="search-input" type="search" placeholder="Search" required>
					</div>

					<div class="form-group">
						<select class="form-control" id="search-option">
							<option selected>Article</option>
							<option>Comment</option>
							<option>User</option>
						</select>
					</div>
					<button class="btn btn-default search" id="search-button"
						type="button">
						<span class="glyphicon glyphicon-search"></span>
					</button>
				</div>
			</div>
		</div>
	</div>
</nav>

<div id="loginModal" class="modal fade" role="dialog">
	<jsp:include page="connect/signin.inc.jsp"></jsp:include>
</div>

<div id="logupModal" class="modal fade" role="dialog">
	<jsp:include page="connect/signup.inc.jsp"></jsp:include>
</div>

<div class="content"></div>
